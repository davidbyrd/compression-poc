# Compression Poc

## Running POC
1. Get an API key from tinypng
2. Verify you have lots of optimizers installed:
    * MacOS: `brew install jpegoptim optipng pngquant gifsicle webp`
    * Dockerfile: `sudo-apt get` the above (see https://github.com/spatie/image-optimizer)
3. Be sure to `composer up` to install all packagist packages.
4. Execute the following command: `TINYPNG_API_KEY=your_api_key php ./poc.php`

   `
