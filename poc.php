<?php

require_once("vendor/autoload.php");

use function Tinify\setKey;
use function Tinify\fromFile;
use Spatie\ImageOptimizer\OptimizerChainFactory;

setKey(getenv('TINYPNG_API_KEY'));

echo "Tinify:\n";
$duration = 0;
$count = 10;
for($i = 1;$i <= $count;$i++ ) {
	$start = time();
	$source = fromFile("tower.png");
	$source->toFile("tinify.png");
	$duration += time() - $start;
}
echo sprintf("Tinify took an average of %s\n", $duration/$count);


$factory = new ImageOptimizer\OptimizerFactory(['ignore_errors' => false]);
$optimizer = $factory->get('smart');

echo "PS:\n";
$duration = 0;
for($i = 1;$i <= $count;$i++ ) {
	$start = time();
	$filepath = "optimizer.png";
	if (!copy("tower.png", $filepath)) {
		echo "failed to copy...\n";
	}
	$optimizer->optimize($filepath);
	$duration += time() - $start;
}
echo sprintf("PS took an average of %s\n", $duration/$count);

echo "Spatie:\n";
$duration = 0;
for($i = 1;$i <= $count;$i++ ) {
	$start = time();
	$optimizerChain = OptimizerChainFactory::create();
	$optimizerChain->optimize("tower.png", "spatie.png");
	$duration += time() - $start;
}
echo sprintf("Spatie took an average of %s\n", $duration/$count);